<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This app has been possible thanks to a post of &lt;a href=&quot;http://nckweb.com.ar/sailing-code/2015/01/05/dconf/&quot;&gt;nckweb.com.ar&lt;/a&gt; where you can see how can read/write values of dconf tool from QML.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Special thanks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Fix it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MotoG Camera Fix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MotoG port camera get stucked sometimes when you choose to use front camera or video.

This is just a simple QML app that changes camera settings to picture mode and rear camera so you can use it again.

The first item in the pull down menu will fix this.

Enjoy! :D</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
