# MotoGCameraFix

MotoG port (falcon, by Piggz) camera get stucked sometimes when you choose to use front camera or video.

This is just a simple QML app that changes camera settings to picture mode and rear camera so you can use it again.

Idea of tuning dconf values came from Andrew Branson and how to do it just using QML from this post:
http://nckweb.com.ar/sailing-code/2015/01/05/dconf/
 
Icon design based on jolla-camera icon. I release this app without any warranty of any kind. I've tested it and it works in my Falcon.

Enjoy! :D
