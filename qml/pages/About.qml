/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: page
    SilicaListView {
        id: listView
        model: 20
        anchors.fill: parent
        Column{
            width: parent.width*0.9
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader {
                title: qsTr("About")
            }
            Image {
                source:"qrc:///res/logo.png"
//                horizontalAlignment: Image.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                width: parent.width
                wrapMode: Text.Wrap
                text: qsTr("This app has been possible thanks to a post of <a href=\"http://nckweb.com.ar/sailing-code/2015/01/05/dconf/\">nckweb.com.ar</a> where you can see how can read/write values of dconf tool from QML.")
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Author")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                text:"Juan Pablo Navarro"
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Special thanks")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                wrapMode: Text.Wrap
                text:"To Andrew Branson who showed me the possibilities of dconf."
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("License")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                text:"Public domain"
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Icon")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                wrapMode: Text.Wrap
                text:"Icon based on Jolla-camera app. I claim no right on this."
            }
        }
    }
}





