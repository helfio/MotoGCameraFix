# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = MotoGCameraFix

CONFIG += sailfishapp

SOURCES += src/MotoGCameraFix.cpp

OTHER_FILES += qml/MotoGCameraFix.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/MotoGCameraFix.changes.in \
    rpm/MotoGCameraFix.spec \
    rpm/MotoGCameraFix.yaml \
    translations/*.ts \
    MotoGCameraFix.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/MotoGCameraFix-de.ts

DISTFILES += \
    qml/pages/About.qml \
    icons/icon-launcher-camera.svg \
    icons/86x86/MotoGCameraFix.png \
    icons/108x108/MotoGCameraFix.png \
    icons/128x128/MotoGCameraFix.png \
    icons/256x256/MotoGCameraFix.png

RESOURCES += \
    resources.qrc

